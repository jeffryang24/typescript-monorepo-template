module.exports = function(api) {
  api.cache.using(() => process.env.NODE_ENV === 'production');

  /************************************
   * Babel Preset(s)
   ************************************/
  const babelPresetEnvironment = [
    '@babel/preset-env',
    {
      useBuiltIns: 'usage',
      corejs: 3,
      modules: false
    }
  ];
  const babelPresetEnvironmentTestEnvironment = [
    '@babel/preset-env',
    {
      useBuiltIns: 'usage',
      corejs: 3
    }
  ];
  const babelPresetReact = [
    '@babel/preset-react',
    {
      development: api.env('development'),
      useBuiltIns: true
    }
  ];
  const babelPresetTypeScript = ['@babel/preset-typescript'];

  /************************************
   * Babel Plugin(s)
   ************************************/
  // Proposal
  const babelPluginPPSClassProperties = [
    '@babel/plugin-proposal-class-properties'
  ];
  const babelPluginPPSNullishCoalescingOperator = [
    '@babel/plugin-proposal-nullish-coalescing-operator'
  ];
  const babelPluginPPSNumericSeparator = [
    '@babel/plugin-proposal-numeric-separator'
  ];
  const babelPluginPPSObjectRestSpread = [
    '@babel/plugin-proposal-object-rest-spread',
    { useBuiltIns: true }
  ];
  const babelPluginPPSOptionalChaining = [
    '@babel/plugin-proposal-optional-chaining'
  ];

  // Stable
  const babelPluginMacros = ['babel-plugin-macros'];
  const babelPluginSyntaxDynamicImport = [
    '@babel/plugin-syntax-dynamic-import'
  ];
  const babelPluginTransformRuntime = [
    '@babel/plugin-transform-runtime',
    {
      corejs: 3,
      helpers: true,
      regenerator: true,
      useESModules: api.env('development') || api.env('production'),
      version: '^7.8.4'
    }
  ];

  /************************************
   * Babel Config(s)
   ************************************/
  const config = {
    comments: false
  };

  /************************************
   * Summaries
   ************************************/
  const plugins = [
    ...babelPluginPPSClassProperties,
    babelPluginPPSObjectRestSpread,
    ...babelPluginPPSNumericSeparator,
    ...babelPluginPPSOptionalChaining,
    ...babelPluginPPSNullishCoalescingOperator,
    ...babelPluginSyntaxDynamicImport,
    babelPluginTransformRuntime,
    ...babelPluginMacros
  ];
  const pluginsTestEnvironment = [
    ...babelPluginPPSClassProperties,
    babelPluginPPSObjectRestSpread,
    ...babelPluginPPSNumericSeparator,
    ...babelPluginPPSOptionalChaining,
    ...babelPluginPPSNullishCoalescingOperator,
    ...babelPluginSyntaxDynamicImport,
    babelPluginTransformRuntime,
    ...babelPluginMacros
  ];

  const presets = [
    ...babelPresetTypeScript,
    babelPresetReact,
    babelPresetEnvironment
  ];
  const presetsTestEnvironment = [
    ...babelPresetTypeScript,
    babelPresetReact,
    babelPresetEnvironmentTestEnvironment
  ];

  /************************************
   * Environment
   ************************************/
  const environment = {
    test: {
      plugins: pluginsTestEnvironment,
      presets: presetsTestEnvironment
    }
  };

  /************************************
   * Return
   ************************************/
  return {
    ...config,
    plugins,
    presets,
    env: environment
  };
};
