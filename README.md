# Typescript Monorepo Template

A simple typescript monorepo template for your new project.

## Table of Content(s) 📑

- [Typescript Monorepo Template](#typescript-monorepo-template)
  - [Table of Content(s) 📑](#table-of-contents-%f0%9f%93%91)
  - [Feature(s) 🎉](#features-%f0%9f%8e%89)
  - [Installation 🚦](#installation-%f0%9f%9a%a6)
    - [git clone](#git-clone)
    - [degit (Recommended!)](#degit-recommended)
  - [Contributing Guide 📝](#contributing-guide-%f0%9f%93%9d)
  - [License](#license)

## Feature(s) 🎉

- Lerna w/ yarn workspace.
- Unit testing with jest.
- ESLint with XO style.
- Commitizen friendly.
- Support babel macro.
- And many more, see roadmap [here][milestone].

## Installation 🚦

### git clone

You can use git clone to clone this project for your new project boilerplate.

```shell
# Clone using ssh
git clone git@gitlab.com:stickey/typescript-monorepo-template.git my-new-app

# Clone using https
git clone https://gitlab.com/stickey/typescript-monorepo-template.git my-new-app

# Clone specific tag (using ssh)
git clone --branch 1.0.0-beta git@gitlab.com:stickey/typescript-monorepo-template.git my-new-app
```

After cloning the repository, please remember to change the remote to your own repository. Also, you could re-init the cloned repository by removing the `.git` directory and execute `git init` again.

### degit (Recommended!)

You can also use [`degit`][degit-link] to bootstrap your new project automatically without hassle.

```shell
# Install degit globally using yarn
yarn global add degit

# Or Install degit globally using npm
npm install -g degit

# Degit this repo (Please choose one of below technique)
degit gitlab:stickey/typescript-monorepo-template my-new-app
degit git@gitlab.com:stickey/typescript-monorepo-template.git my-new-app
degit https://gitlab.com/stickey/typescript-monorepo-template.git my-new-app
# Degit specific version
degit gitlab:stickey/typescript-monorepo-template#1.0.0-beta my-new-app
```

## Contributing Guide 📝

Please see our [contributing guide](CONTRIBUTING.md) here.

## License

Licensed under [MIT](LICENSE).

<!-- Links -->

[degit-link]: https://www.npmjs.com/package/degit
[milestone]: https://gitlab.com/stickey/typescript-monorepo-template/-/milestones
