const path = require('path');

module.exports = {
  webpackFinal: async config => {
    const newConfig = {
      ...config,
      module: {
        ...config.module,
        rules: [
          ...config.module.rules,
          {
            test: /\.tsx?$/,
            use: [
              {
                loader: require.resolve('babel-loader'),
                options: {
                  rootMode: 'upward'
                }
              },
              require.resolve('react-docgen-typescript-loader')
            ],
            include: path.resolve(__dirname, '../src')
          }
        ]
      },
      resolve: {
        ...config.resolve,
        extensions: [...(config.resolve.extensions || []), '.ts', '.tsx']
      }
    };

    return newConfig;
  },
  addons: [
    '@storybook/addon-knobs/register',
    '@storybook/addon-actions/register',
    '@storybook/addon-viewport/register',
    '@storybook/addon-docs'
  ]
};
