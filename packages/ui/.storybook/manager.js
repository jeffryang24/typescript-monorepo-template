import { addons } from '@storybook/addons';

import defaultTheme from './themes/default';

addons.setConfig({
  theme: defaultTheme
});
