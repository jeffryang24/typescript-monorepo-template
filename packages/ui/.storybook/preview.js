import { addParameters, configure } from '@storybook/react';

function loaderFn() {
  const allExports = new Set([require('../src/welcome.stories.mdx')]);
  const requireContext = require.context(
    '../src',
    true,
    /\.stories\.(tsx|mdx)$/
  );
  requireContext
    .keys()
    .forEach(fileName => allExports.add(requireContext(fileName)));
  return [...allExports];
}

addParameters({
  docs: {
    inlineStories: false
  }
});
configure(loaderFn, module);
