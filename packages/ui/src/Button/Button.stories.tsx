import React, { FunctionComponent } from 'react';

import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';

import { Button } from '@storybook/react/demo';

export default { title: 'Button', decorators: [withKnobs] };

export const withText: FunctionComponent = () => (
  <Button onClick={action('onClick')}>
    {text('children', 'Hello Button')}
  </Button>
);

export const withEmoji: FunctionComponent = () => (
  <Button>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
);
